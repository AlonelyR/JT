#include<bits/stdc++.h>
using namespace std;
bool judge(int n){
    if(n<=1)return false;
    if(n==2)return true;

    for(int i=2;i<n;i++){
        if(n%i==0)
            return false;
    }
    return true;
}
int main(){
    int c;cin>>c;
    for(int i=2;i<c;i++){
        if(judge(i)&&c%i==0){
            cout<<c/i<<endl;
            break;
        }
    }
    return 0;
}