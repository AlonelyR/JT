//括号匹配问题
#include<bits/stdc++.h>
using namespace std;

int main(){
    string s;
    cin>>s;

    if(s[0]==')'){
        printf("NO");
        return 0;
    }

    int m=0;
    for(char c:s){
        if(c=='(')m++;
        if(c==')')m--;
        if(m<0){
            printf("NO");
            return 0;
        }
        if(c=='@'){
            if(m==0)printf("YES");
            else printf("NO");
        }
    }
}