#include<bits/stdc++.h>
using namespace std;

int main(){
    ios::sync_with_stdio(false);
    cout.tie(NULL);
    cin.tie(NULL);

    int n;cin>>n;
    // vector<vector<int>>nums(n+1,vector<int>(n+1,0));
    vector<vector<int>>prefix(n+1,vector<int>(n+1,0));
    //输入
    for(int i=1;i<=n;++i){
        for(int j=1;j<=n;j++){
            cin>>prefix[i][j];
        }
    }

    //前缀和矩阵
    for(int i=1;i<=n;i++)
        for(int j=1;j<=n;j++)prefix[i][j]+=prefix[i][j-1];
    for(int j=1;j<=n;j++)
        for(int i=1;i<=n;i++)prefix[i][j]+=prefix[i-1][j];

    //遍遍历前缀和求最小值
    int ans = INT_MIN;
    for(int i=1;i<=n;i++)
        for(int j=1;j<=n;j++)
            for(int k=i;k<=n;k++)
                for(int l=j;l<=n;l++){
                    int a = prefix[k][l]-prefix[k-i][l]-prefix[k][l-j]+prefix[k-i][l-j];
                    ans = max(ans,a);
                }
    cout<<ans; 
    return 0;
}