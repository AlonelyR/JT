package LQ;
import java.util.*;
public class _43P1540 {
    public static void main(String[] args){
        int n,m;
        Scanner sc = new Scanner(System.in);
        m = sc.nextInt();
        n = sc.nextInt();

        int k,ans=0;
        //队列
        Queue<Integer> qu =  new LinkedList<>();
        for(int i=0;i<n;i++){
            k = sc.nextInt();
            //查询不存在
            if(!qu.contains(k)){
                ans++;
                qu.offer(k);
                if(qu.size()>m)qu.poll();//超出弹出最头的元素
            }
        }
        System.out.println(ans);
        sc.close();
    }

}
