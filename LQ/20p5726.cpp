#include<bits/stdc++.h>
using namespace std;
int main(){
    int c;cin>>c;
    double mx = INT_MIN,mn=INT_MAX;
    double sum=0;
    for(int i=0;i<c;i++){
        double k;cin>>k;
        mx = max(mx,k);
        mn = min(mn,k);
        sum+= k;
    }
    sum=(sum-mx-mn)/(c-2);
    cout<<fixed<<setprecision(2)<<sum;
    return 0;
}