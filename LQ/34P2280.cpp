#include<bits/stdc++.h>
const int M=5010;
using namespace std;

int nums[M][M]={0};
int main(){
    ios::sync_with_stdio(false);
    cout.tie(NULL);cin.tie(NULL);

    int n,m;
    cin>>n>>m;
    for(size_t i=0;i<n;i++){
        int x,y,v;
        cin>>y>>x>>v;
        nums[x+1][y+1]+=v;
    }
    int N = 5001;
   //整个求前缀和 
//    for(int i=1;i<=M-1;i++){
//        for(int j=1;j<=M-1;j++){
//            nums[i][j] = nums[i-1][j]+nums[i][j-1]-nums[i-1][j-1]+nums[i][j];
//        }
//    }

    //列前缀和
    for(int i=1;i<=N;i++)
        for(int j=1;j<=N;j++)nums[i][j] += nums[i][j-1];
        
    //行前缀和
    for(int j=1;j<=N;j++)
        for(int i=1;i<=N;i++)nums[i][j] += nums[i-1][j];

    int ans = INT_MIN;//求最大值取最小值 
    for(int i=m;i<=N;i++){
        for(int j=m;j<=N;j++){
            //求区域的价值
            int k =nums[i][j]-nums[i-m][j]-nums[i][j-m]+nums[i-m][j-m];
            ans = max(k,ans);
        }
    }
    cout<<ans;
    return 0;
}