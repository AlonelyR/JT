#include<bits/stdc++.h>
using namespace std;
const int M=1e5+10;
bool check(int x,int n,int k,vector<int>&a){
    int ans = 0;
    for(int i=0;i<n;i++){
        ans+=a[i]/x;
        if(ans>=k)return true;
    }
    return ans>=k;
}
int main(){
    ios::sync_with_stdio(false);
    cout.tie(NULL);cin.tie(NULL);

    int n,k;cin>>n>>k;
    vector<int>a(M,0);
    for(int i=0;i<n;i++)cin>>a[i];

    int l=1,r = 1e8,mid;
    //防止数据过大溢出
    long long int sum=0;

    sum = accumulate(a.begin(),a.end(),0LL);
    if(sum<k){
        cout<<0<<endl;
        return 0;
    }

    while (l<r)
    {
        int mid = (l+r+1)>>1;
        if(check(mid,n,k,a)){
            l=mid;
        }else r = mid-1;
    }
    cout<<l;
    
    return 0;
}