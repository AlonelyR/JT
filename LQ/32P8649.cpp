#include<bits/stdc++.h>
using namespace std;
int main(){
    ios::sync_with_stdio(false);
    cout.tie(NULL);cin.tie(NULL);

    int n,k;
    cin>>n>>k;
    int ans=0;
    map<int,int>mp;
    mp[0] = 1;

    for(size_t i=1;i<=n;i++){
        int s;cin>>s;
        ans+=s;
        mp[ans%k]++;
        ans%=k;//不影响ans+s
    }
    ans= 0;
    for(int i=0;i<k;i++){
        ans+=(mp[i]-1)*mp[i]/2;
    }
    cout<<ans<<endl;
    return 0;
}