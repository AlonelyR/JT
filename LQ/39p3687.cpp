#include<bits/stdc++.h>
using namespace std;
const int M = 1e5+10;

int check(int x,int n,int k,vector<int>&a,vector<int>&b){
    int ans=0;
    //求块数
    for(int i=0;i<n;i++)ans+=(a[i]/x)*(b[i]/x);
    return ans>=k;//达到需要的数目
}

int main(){
    ios::sync_with_stdio(false);
    cout.tie(NULL);
    cin.tie(NULL);

    //输入
    vector<int>a(M),b(M);
    int n,k;cin>>n>>k;
    for(int i=0;i<n;i++)cin>>a[i]>>b[i];

    int l=1,r=1e5;
    while (l<r)
    {
        int mid = (r+l+1)>>1;//求平均 等价/2
        if(check(mid,n,k,a,b)){
            l = mid;
        }else r  = mid-1;//边长过大 没有达到
    }
    cout<<l; 
}