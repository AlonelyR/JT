#include<bits/stdc++.h>
using namespace std;
void cal(int x1,int y1,int x2,int y2,vector<vector<int>>&nums){
    int n = nums.size()-1; 
    for(int i=y1;i<=y2;i++){
        nums[i][x1]+=1;
        if(x2<n)nums[i][x2+1]-=1;
    }
}
int main(){
    ios::sync_with_stdio(false);
    cout.tie(NULL);cin.tie(NULL);

    int n,m;
    cin>>n>>m;
    vector<vector<int>>nums(n+1,vector<int>(n+1,0));
    for(size_t i=0;i<m;i++){
        int x1,y1,x2,y2;
        cin>>y1>>x1>>y2>>x2;
        cal(x1,y1,x2,y2,nums);
    }
    vector<vector<int>>ans(n+1,vector<int>(n+1,0));
    for(int i=1;i<=n;i++){
        for(int j=1;j<=n;j++){
            ans[i][j] = ans[i][j-1]+nums[i][j];
        }
    }

    for(int i=1;i<=n;i++){
        for(int j=1;j<=n;j++){
            cout<<ans[i][j]<<" ";
        }
        cout<<endl;
    }
    return 0;
}