package LQ;
import java.util.*;
public class _45P10448 {

    public static void main(String[] args) {
       Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();//输入数字
        int n = sc.nextInt();//输入长度
        int[] nums = new int[n+1];

        int now = 1,l = 1;
        deal(nums, m, n, now,l);
        sc.close();

    }

    public static void deal(int[] nums,int m,int n,int now,int l){
        if(now==n+1){
            for(int i=1;i<=n;i++){
                System.out.print(nums[i]+" ");
            }
            System.out.println();
            return;
        }

        for(int i=l;i<=m-n+now;i++){
            nums[now] = i;
            deal(nums,m,n,now+1,i+1);
        }
    }
}
