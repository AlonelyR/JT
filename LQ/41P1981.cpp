#include<bits/stdc++.h>
using namespace std;
int main(){
    ios::sync_with_stdio(false);
    cout.tie(NULL);
    cin.tie(NULL);

    int mod = 10000;
    stack<int> st;
    long long int c; 
    char d;
    cin>>c;
    c%=mod;
    st.emplace(c);
    while(cin>>d>>c){
        if(d=='*'){
            int l = st.top();
            st.pop();
            c%=mod;
            st.emplace(l*c%mod);
        }else{
            st.emplace(c%mod);
        }

    }

    while (st.size()!=1)
    {
       int l = st.top();
       st.pop();
       int r = st.top();
       st.pop();
       st.emplace((l+r)%mod);
    }
    cout<<st.top();
    return 0;
}