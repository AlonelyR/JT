#include<bits/stdc++.h>
using namespace std;
void cal(vector<long long int>&nums,vector<long long int>&prefix,long long int &ans){
    int n = nums.size()-1;
    //求和
    for(int i=1;i<=n-1;i++){
        ans+=nums[i]*prefix[n-i];
    }
}
int main(){
    ios::sync_with_stdio(false);
    cout.tie(NULL);
    cin.tie(NULL);

    int n;cin>>n;
    vector<long long int>nums(n+1,0);
    vector<long long int>prefix(n+1,0);
    long long int ans=0;
    for(int i=1;i<=n;i++){
        cin>>nums[i];
    }

    //求an-a2的前缀和
    for (int i = 1; i <=n-1; i++)
    {
        prefix[i] = prefix[i-1] + nums[n+1-i];
    }

    cal(nums,prefix,ans);
    cout<<ans<<endl;
    return 0;
}