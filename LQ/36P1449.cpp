//前缀和

#include<bits/stdc++.h>
using namespace std;
bool is_num(string &s){
    for(char c:s){
        if(!isdigit(c))return false;
    }
    return true;
}

int cal(char c,int a,int b){
    if(c=='+')return a+b;
    else if(c=='-')return a-b;
    else if(c=='*')return a*b;
    else if(c=='/')return a/b;
    return -1;
}

void cal2(stack<int>&st,string &num,int &m){
    bool use_m=false;
     if(is_num(num)){
         int k = stoi(num);
         st.push(k);
     }else{
         for(char c:num){
            //先弹出是右操作数
             if(c=='+'||c=='-'||c=='*'||c=='/'){
                 int r = st.top();
                 st.pop();
                 int l =  st.top();
                 st.pop();
                 st.push(cal(c,l,r));
             }else{
                use_m = true;
                 m=m*10+int(c-'0');
             }
         }
         if(use_m){
            st.push(m);
            m=0;
         }
     }
}

int main(){
    string s;
    cin>>s;
    int n=0,m=0;
    int end = s.find('@');
    stack<int>st; 
    while(true){
        int pos = s.find('.',n);
        if(pos==-1)break;
        string num = s.substr(n,pos-n);

        cal2(st,num,m);
        n = pos+1;
    }

    string num = s.substr(n,end-n);
    cal2(st,num,m);
    cout<<st.top();
    return 0;
}