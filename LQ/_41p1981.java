package LQ;

import java.util.*;

public class _41p1981{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int mod = 10000;
        Stack<Integer> st = new Stack<>();

        // 读取整个输入
        String input = sc.nextLine();
        sc.close();

        // 解析输入
        String[] tokens = input.split("(?<=[+*])|(?=[+*])"); // 按符号分割
        long c = Long.parseLong(tokens[0]) % mod;
        st.push((int) c);

        for (int i = 1; i < tokens.length; i += 2) {
            char op = tokens[i].charAt(0); // 操作符
            c = Long.parseLong(tokens[i + 1]); // 数字
            if (op == '*') {
                int l = st.pop();
                c %= mod;
                st.push((int) (l * c % mod));
            } else if (op == '+') {
                st.push((int) (c % mod));
            }
        }

        while (st.size() > 1) {
            int l = st.pop();
            int r = st.pop();
            st.push((l + r) % mod);
        }
        System.out.println(st.pop());
    }
}
