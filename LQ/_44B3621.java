package LQ;
import java.util.*;

public class _44B3621 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();//元组长度
        int n = sc.nextInt();
        int now = 0;
        int[] ans = new int[m];
        show(n,m,now,ans);
        sc.close();
    }

    public static void show(int n,int m,int now,int[] ans){
        if(now==m){
            for(int i=0;i<m;i++){
                System.out.print(ans[i]+" ");
            }
            System.out.println();
            return;
        }
        
        for(int i=1;i<=n;i++){
            ans[now] = i;
            show(n,m,now+1,ans);
            ans[now] = 0;
        }
        
    }
}