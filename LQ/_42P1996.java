package LQ;

import java.util.Scanner;

//import java.util.*;

public class _42P1996 {

    public static void main(String[] args) {
        //读取整数
        int n,m;
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        m = sc.nextInt();

        int[] record = new int[200];
        int r = 0;
        for(int i=0;i<n;i++){
            //循环m输出
            for(int j=0;j<m;j++){
                if(++r>n)r = 1;
                if(record[r]==1)j--;
            }
            System.out.print(r+" ");
            record[r]  = 1;
        }

        sc.close();
    }
}